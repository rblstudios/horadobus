package br.com.rblstudios.horadobus.service.`interface`

import br.com.rblstudios.horadobus.model.BusLineShape
import br.com.rblstudios.horadobus.model.BusLines
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by renanlucas on 08/03/18.
 */
interface BusLineServiceInterface {
    @GET("linhas")
    fun getLinhas(@Header("Authorization") token: String): Call<List<BusLines>>

    @GET("shapelinha")
    fun getShapeLinha(@Header("Authorization") token: String, @Query("linha")  busLine: String): Call<List<BusLineShape>>
}