package br.com.rblstudios.horadobus.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.rblstudios.horadobus.Network.RetrofitInitializer
import br.com.rblstudios.horadobus.R
import br.com.rblstudios.horadobus.adapter.LinhaListAdapter
import br.com.rblstudios.horadobus.model.BusLines
import kotlinx.android.synthetic.main.activity_linhas.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.app.Activity
import android.content.Context
import android.util.Log
import br.com.rblstudios.horadobus.util.HoraDoBusUtil.buscarTokenAcessoAPI

class LinhasFragment : Fragment() {

    var secondActivityFragment: MapFragment? = null
    lateinit var busLinesRecycler: RecyclerView
    var busLinesList: List<BusLines> = ArrayList()
    var busLineClickCallback: BusLineClicked? = null

    interface BusLineClicked {
        fun updateMapWithBusLine(busLine: String, fragment: MapFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val activity = context as? Activity
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            busLineClickCallback = activity as BusLineClicked?
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement TextClicked")
        }
    }

    override fun onDetach() {
        busLineClickCallback = null // => avoid leaking
        super.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.fragment_linhas, container, false)
        busLinesRecycler = inf.findViewById<RecyclerView>(R.id.busLinesRecycler)
        return inf
    }

    override fun onResume() {
        loadBusLines(busLinesRecycler)
        super.onResume()
    }

    private fun loadBusLines(busLinesRecycler: RecyclerView) {
        val linhasCall = RetrofitInitializer().busLineService().getLinhas("Bearer " + buscarTokenAcessoAPI())
        linhasCall.enqueue(object : Callback<List<BusLines>?> {
            override fun onResponse(call: Call<List<BusLines>?>?,
                                    response: Response<List<BusLines>?>?) {
                response?.body()?.let {
                    busLinesList = it

                    busLinesRecycler.let {
                        it.adapter = LinhaListAdapter(secondActivityFragment, busLinesList, context, busLineClickCallback)
                        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
                        it.layoutManager = layoutManager
                    }
                }
            }

            override fun onFailure(call: Call<List<BusLines>?>?,
                                   t: Throwable?) {
                Log.e("onFailureError", t?.message)
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        secondActivityFragment = fragment_map_large as MapFragment?
    }
}// Required empty public constructor
