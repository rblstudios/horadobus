package br.com.rblstudios.horadobus.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.rblstudios.horadobus.Network.RetrofitInitializer
import br.com.rblstudios.horadobus.R
import br.com.rblstudios.horadobus.model.BusLines
import br.com.rblstudios.horadobus.util.HoraDoBusUtil.buscarTokenAcessoAPI
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.os.SystemClock
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import br.com.rblstudios.horadobus.service.DangerousLocationUserWarningService


class LoadingActivity : AppCompatActivity() {

    private var am: AlarmManager? = null
    private var piServicoLocalizacao: PendingIntent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        carregarDados()

        val alarmeFuncionando = PendingIntent.getBroadcast(this, 0, Intent(this, DangerousLocationUserWarningService::class.java),
                PendingIntent.FLAG_NO_CREATE) != null

        if (!alarmeFuncionando) {
            // Inicia serviço de localização automático
            iniciarServicoAutomaticoLocalizacao()
        }
    }

    override fun onResume() {
        super.onResume()

        val alarmeFuncionando = PendingIntent.getBroadcast(this, 0, Intent(this, DangerousLocationUserWarningService::class.java),
                PendingIntent.FLAG_NO_CREATE) != null

        if (!alarmeFuncionando) {
            iniciarServicoAutomaticoLocalizacao()
        }
    }

    private fun carregarDados() {
        val linhasCall = RetrofitInitializer().busLineService().getLinhas("Bearer " + buscarTokenAcessoAPI())
        linhasCall.enqueue(object : Callback<List<BusLines>?> {
            override fun onResponse(call: Call<List<BusLines>?>?,
                                    response: Response<List<BusLines>?>?) {
                response?.body()?.let {
                    val busLinesList: List<BusLines> = it
                    openMainActivity(busLinesList)
                }
            }

            override fun onFailure(call: Call<List<BusLines>?>?,
                                   t: Throwable?) {
                runOnUiThread {
                    val builder = AlertDialog.Builder(this@LoadingActivity)
                    builder.setTitle("Erro de conexão")
                    builder.setMessage("Não foi possível buscar as linhas de ônibus. Você gostaria de tentar novamente?")
                    builder.setPositiveButton("Sim") { dialog, which ->
                        carregarDados()
                    }

                    builder.setNegativeButton("No") { dialog, which ->
                        finish()
                    }

                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        })
    }

    private fun openMainActivity(busLinesList: List<BusLines>) {
        val intent = Intent(this, LinhasActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun iniciarServicoAutomaticoLocalizacao() {
        val servico = Intent(this, DangerousLocationUserWarningService::class.java)
        piServicoLocalizacao = PendingIntent.getService(this, 0, servico, 0)
        am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // Cancela o alarme anterior
        am?.cancel(piServicoLocalizacao)
        am?.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(), 60000, piServicoLocalizacao)
    }
}
