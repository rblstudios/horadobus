package br.com.rblstudios.horadobus.ui

import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import br.com.rblstudios.horadobus.Network.RetrofitInitializer
import br.com.rblstudios.horadobus.R
import br.com.rblstudios.horadobus.model.BusLineShape
import br.com.rblstudios.horadobus.model.DangerousLocation
import br.com.rblstudios.horadobus.util.DataUtil
import br.com.rblstudios.horadobus.util.HoraDoBusUtil.buscarTokenAcessoAPI
import br.com.rblstudios.horadobus.util.enumerador.FormatoData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.FirebaseError
import com.google.firebase.database.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLongClickListener {

    private lateinit var database: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private var busLine: String = ""
    private val DANGEROUS_LOCATIONS_CHILD = "DANGEROUS_LOCATIONS"
    private val RADIUS: Double = 50.0

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.fragment_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (this.intent.hasExtra("busLine")) {
            busLine = this.intent.getStringExtra("busLine")
        }

        // Get DatabaseInstance and Reference
        database = FirebaseDatabase.getInstance()
        databaseReference = database.reference
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.getUiSettings().setZoomControlsEnabled(true)
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapLongClickListener(this)
        setUpMap()
        getBusLineShape(busLine)

        // Add a marker in Sydney and move the camera
        //val sydney = LatLng(-34.0, 151.0)
        //mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

        val query = databaseReference.child(DANGEROUS_LOCATIONS_CHILD)
        // My top posts by number of stars
        query.addValueEventListener(object : ValueEventListener {

            override fun onCancelled(error: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snapshot: DataSnapshot?) {
                val children = snapshot!!.children
                children.forEach {
                    val dangerousLocation = it.value as HashMap<String, Any>
                    val point = LatLng(dangerousLocation["lat"] as Double, dangerousLocation["lon"] as Double)

                    mMap.addCircle(CircleOptions()
                            .center(point)
                            .radius((dangerousLocation["radius"] as Long).toDouble())
                            .strokeWidth(0f)
                            .fillColor(0x55F4BF42))
                }
            }
        })
    }

    override fun onMapLongClick(point: LatLng) {
        val marker = mMap.addMarker(MarkerOptions().position(point).title(point.toString()))
        runOnUiThread {
            val builder = AlertDialog.Builder(this@MapActivity)
            builder.setTitle("Erro de conexão")
            builder.setMessage("Esta região é perigosa?")
            builder.setPositiveButton("Sim") { dialog, which ->
                marker.remove()

                //val id = databaseReference.child(DANGEROUS_LOCATIONS_CHILD).push().key
                val dangerousLocation = DangerousLocation(point.latitude, point.longitude, RADIUS, DataUtil.buscaDataHoraAtual(FormatoData.FORMATO_DATAHORA_PADRAO), busLine)
                databaseReference.child(DANGEROUS_LOCATIONS_CHILD).push().setValue(dangerousLocation)

                mMap.addCircle(CircleOptions()
                        .center(point)
                        .radius(RADIUS)
                        .strokeWidth(0f)
                        .fillColor(0x55F4BF42))
            }

            builder.setNegativeButton("Não") { dialog, which ->
                marker.remove()
            }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }

    var polyline: Polyline? = null
    private fun getBusLineShape(busLine: String) {
        val busLineShapeCall = RetrofitInitializer().busLineService().getShapeLinha("Bearer " + buscarTokenAcessoAPI(), busLine)
        busLineShapeCall.enqueue(object : Callback<List<BusLineShape>?> {
            override fun onResponse(call: Call<List<BusLineShape>?>?,
                                    response: Response<List<BusLineShape>?>?) {
                response?.body()?.let {
                    val busLineShapeList: List<BusLineShape>? = it

                    if (busLineShapeList != null) {
                        // declare bounds object to fit whole route in screen
                        val LatLngBounds = LatLngBounds.Builder()


                        val polylineOptions = PolylineOptions()
                        busLineShapeList.forEach { x ->
                            polylineOptions
                                    .add(LatLng(x.LAT.replace(",", ".").toDouble(), x.LON.replace(",", ".").toDouble()))
                                    .width(7f)
                                    .color(resources.getColor(R.color.urbs_blue))
                        }

                        for (point in polylineOptions.points) {
                            LatLngBounds.include(point)
                        }

                        if (polyline != null) {
                            polyline!!.remove()
                        }

                        // build bounds
                        val bounds = LatLngBounds.build()
                        polyline = mMap.addPolyline(polylineOptions)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                        Toast.makeText(applicationContext, "Updated map with Bus Line " + busLine, Toast.LENGTH_LONG).show()
                        supportActionBar!!.setTitle("Bus Line " + busLine)
                    } else {
                        Toast.makeText(applicationContext, "It was not possible to update map with Bus Line " + busLine, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<BusLineShape>?>?,
                                   t: Throwable?) {
                runOnUiThread {
                    val builder = AlertDialog.Builder(this@MapActivity)
                    builder.setTitle("Erro de conexão")
                    builder.setMessage("Não foi possível buscar esta linha de ônibus. Você gostaria de tentar novamente?")
                    builder.setPositiveButton("Sim") { dialog, which ->
                        getBusLineShape(busLine)
                    }

                    builder.setNegativeButton("No") { dialog, which ->
                        finish()
                    }

                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        })
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        mMap.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13f))
            }
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        setUpMap()
        return true
    }
}
