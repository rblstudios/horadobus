package br.com.rblstudios.horadobus.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.rblstudios.horadobus.R
import br.com.rblstudios.horadobus.fragment.LinhasFragment
import br.com.rblstudios.horadobus.fragment.MapFragment
import br.com.rblstudios.horadobus.model.BusLines
import br.com.rblstudios.horadobus.ui.MapActivity
import kotlinx.android.synthetic.main.item_list_linha.view.*

/**
 * Created by renanlucas on 07/03/2018.
 */
class LinhaListAdapter(val secondActivityFragment: MapFragment?,
                       private val busLines: List<BusLines>,
                       private val context: Context?,
                       mCallback: LinhasFragment.BusLineClicked?) : Adapter<LinhaListAdapter.ViewHolder>() {
    var busLineClickCallback: LinhasFragment.BusLineClicked? = mCallback

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val busLine = busLines[position]
        holder.let {
            it.nome.text = busLine.NOME
            it.cod.text = busLine.COD

            var color : Int = context!!.resources.getColor( R.color.urbs_gray)
            when(busLine.NOME_COR) {
                "AMARELA" -> color = context.resources.getColor(R.color.urbs_yellow)
                "VERDE" -> color = context.resources.getColor(R.color.urbs_green)
                "PRATA" -> color = context.resources.getColor(R.color.urbs_gray)
                "VERMELHA" -> color = context.resources.getColor(R.color.urbs_red)
                "LARANJA" -> color = context.resources.getColor(R.color.urbs_orange)
                "AZUL" -> color = context.resources.getColor(R.color.urbs_blue)
            }

            it.cod.setTextColor(color)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_list_linha, parent, false)
        view.setOnClickListener {
            if (secondActivityFragment == null) {
                val intent = Intent(context, MapActivity::class.java)
                intent.putExtra("busLine", view.findViewById<TextView>(R.id.linha_cod).text.toString())
                startActivity(context!!, intent, null)
            } else {
                busLineClickCallback!!.updateMapWithBusLine(view.findViewById<TextView>(R.id.linha_cod).text.toString(), secondActivityFragment);
            }
        }
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nome = itemView.linha_nome
        val cod = itemView.linha_cod
    }

    override fun getItemCount(): Int {
        return busLines.size
    }
}

