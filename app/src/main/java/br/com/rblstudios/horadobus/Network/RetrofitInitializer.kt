package br.com.rblstudios.horadobus.Network

import br.com.rblstudios.horadobus.service.`interface`.BusLineServiceInterface
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by renanlucas on 08/03/18.
 */
class RetrofitInitializer {

    private val BASE_URL = "http://apihoradobus.azurewebsites.net/"

    private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun busLineService() = retrofit.create(BusLineServiceInterface::class.java)
}