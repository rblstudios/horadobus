package br.com.rblstudios.horadobus.fragment

import android.support.v4.app.Fragment
import android.util.Log
import com.google.android.gms.maps.SupportMapFragment

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MapFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MapFragment : SupportMapFragment() {

    fun updateMap(busLine: String) {
        Log.d("updateMap:", "Map updated with busLine " + busLine)
    }
}// Required empty public constructor
