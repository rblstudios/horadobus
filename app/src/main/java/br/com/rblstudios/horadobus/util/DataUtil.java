package br.com.rblstudios.horadobus.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.com.rblstudios.horadobus.R;
import br.com.rblstudios.horadobus.util.enumerador.FormatoData;

/**
 * Criado por renan.lucas em 10/05/2017.
 */

public class DataUtil {

    private static final String FORMATO_DATA_BANCO = "yyyy-MM-dd";
    private static final String FORMATO_DATAHORA_BANCO = "yyyy-MM-dd HH:mm:ss";
    private static final String FORMATO_DATA_PADRAO = "dd/MM/yyyy";
    private static final String FORMATO_DATAHORA_PADRAO = "dd/MM/yyyy HH:mm:ss";
    private static final String FORMATO_DATAHORA_NUMERO_PEDIDO = "yyyyMMddHHmmss";
    private static final String FORMATO_CALENDARIO = "EEE MMM dd HH:mm:ss z yyyy";

    /**
     * Baseado na data do calendário do Android retorna o dia da semana já em texto
     * Ex: Segunda-feira -> "2"
     * Ex: Terça-feira -> "3"
     *
     * @return número do dia da semana em texto
     */
    public static String buscaNumeroDiaDaSemanaAtual() {
        Calendar calendario = Calendar.getInstance();
        return String.valueOf(calendario.get(Calendar.DAY_OF_WEEK));
    }

    /**
     * Baseado na data do calendário do Android retorna o dia da semana por extenso
     * Ex: 2 -> "Segunda-Feira"
     * Ex: 3 -> "Terça-Feira"
     *
     * @return Retorna data atual por extenso
     */
    public static String buscaNomeDiaDaSemanaAtual(Context ctx) {
        // Busca número do dia atual da semana e traduz para português
        switch (buscaNumeroDiaDaSemanaAtual()) {
            case "1":
                return ctx.getString(R.string.title_domingo);
            case "2":
                return ctx.getString(R.string.title_segundafeira);
            case "3":
                return ctx.getString(R.string.title_tercafeira);
            case "4":
                return ctx.getString(R.string.title_quartafeira);
            case "5":
                return ctx.getString(R.string.title_quintafeira);
            case "6":
                return ctx.getString(R.string.title_sextafeira);
            case "7":
                return ctx.getString(R.string.title_sabado);
            default:
                return "Algum bug maluco aconteceu";
        }
    }

    /**
     * Busca data do aparelho no formato especificado
     *
     * @return data
     */
    public static String buscaDataAtual(FormatoData formato) {
        String formatoDestinoString;
        String resultado;
        switch (formato) {
            case FORMATO_DATA_PADRAO:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
            case FORMATO_DATA_BANCO:
                formatoDestinoString = FORMATO_DATA_BANCO;
                break;
            default:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
        }

        SimpleDateFormat formatoDestino = new SimpleDateFormat(formatoDestinoString, Locale.getDefault());
        Calendar calendario = buscaCalendario();
        Date data = calendario.getTime();

        try {
            // Retorna a data em formato de data
            resultado = formatoDestino.format(data);
        } catch (Exception e) {
            resultado = "Não foi possível converter a data(" + e.getMessage() + ")";
        }

        return resultado;
    }

    public static Date buscaDataAtual() {
        Calendar calendario = buscaCalendario();
        return calendario.getTime();
    }

    public static Date buscaDataOntem() {
        Calendar calendario = buscaCalendario();
        calendario.add(Calendar.DATE, -1);
        return calendario.getTime();
    }

    public static Date buscaDataPrimeiroDiaMes() {
        Calendar calendarioDataAtual = buscaCalendario();
        calendarioDataAtual.setTime(buscaDataAtual());
        int diaMes = calendarioDataAtual.get(Calendar.DAY_OF_MONTH);
        if (diaMes == 1) {
            Calendar calendario = buscaCalendario();
            calendario.set(Calendar.MONTH, calendarioDataAtual.get(Calendar.MONTH) - 1);
            calendario.set(Calendar.DATE, 1);
            return calendario.getTime();
        } else {
            Calendar calendario = buscaCalendario();
            calendario.set(Calendar.DAY_OF_MONTH, 1);
            return calendario.getTime();
        }
    }

    public static Date buscaDataUltimoDiaMes() {
        Calendar calendario = buscaCalendario();
        calendario.set(Calendar.DATE, calendario.getActualMaximum(Calendar.DATE));
        return calendario.getTime();
    }

    /**
     * Busca data e hora do aparelho em formato "01/01/1999 07:05:19"
     *
     * @return data hora
     */
    public static String buscaDataHoraAtual(FormatoData formato) {
        String formatoDestinoString;
        String resultado;
        switch (formato) {
            case FORMATO_DATAHORA_PADRAO:
                formatoDestinoString = FORMATO_DATAHORA_PADRAO;
                break;
            case FORMATO_DATAHORA_BANCO:
                formatoDestinoString = FORMATO_DATAHORA_BANCO;
                break;
            case FORMATO_DATAHORA_NUMERO_PEDIDO:
                formatoDestinoString = FORMATO_DATAHORA_NUMERO_PEDIDO;
                break;
            default:
                formatoDestinoString = FORMATO_DATAHORA_PADRAO;
                break;
        }

        SimpleDateFormat formatoDestino = new SimpleDateFormat(formatoDestinoString, Locale.getDefault());
        Calendar calendario = buscaCalendario();
        Date data = calendario.getTime();

        try {
            // Retorna a data em formato de data
            resultado = formatoDestino.format(data);
        } catch (Exception e) {
            resultado = "Não foi possível converter a data(" + e.getMessage() + ")";
        }

        return resultado;
    }

    public static Date buscaDataEntrega(String filFat) {

        // Busca data atual
        Calendar calendario = Calendar.getInstance();
        Date data;
        String diaSemana = buscaNumeroDiaDaSemanaAtual();

        switch (filFat) {
            case "010101":
                if (diaSemana.equals("6")) {
                    calendario.add(Calendar.DATE, 3);
                    data = calendario.getTime();
                } else if (diaSemana.equals("7")) {
                    calendario.add(Calendar.DATE, 2);
                    data = calendario.getTime();
                } else {
                    calendario.add(Calendar.DATE, 1);
                    data = calendario.getTime();
                }
                break;

            case "010102":
                if (diaSemana.equals("5") || diaSemana.equals("6")) {
                    calendario.add(Calendar.DATE, 4);
                    data = calendario.getTime();
                } else if (diaSemana.equals("7")) {
                    calendario.add(Calendar.DATE, 3);
                    data = calendario.getTime();
                } else {
                    calendario.add(Calendar.DATE, 2);
                    data = calendario.getTime();
                }
                break;

            default:
                calendario.add(Calendar.DATE, 1);
                data = calendario.getTime();
                break;
        }

        // Retorna data formatada
        return data;
    }

    /**
     * Verifica se o aparelho está com horário automático.
     *
     * @param ctx Contexto
     * @return Returna "true" se o horário estiver automático
     */
    public static boolean verificaHoraAutomatica(Context ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(ctx.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(ctx.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static String formataData(String stringData, FormatoData formatoOrigem, FormatoData formatoDestino) {
        String formatoOrigemString;
        String formatoDestinoString;

        switch (formatoOrigem) {
            case FORMATO_DATA_BANCO:
                formatoOrigemString = FORMATO_DATA_BANCO;
                break;
            case FORMATO_DATA_PADRAO:
                formatoOrigemString = FORMATO_DATA_PADRAO;
                break;
            case FORMATO_DATAHORA_BANCO:
                formatoOrigemString = FORMATO_DATAHORA_BANCO;
                break;
            case FORMATO_DATAHORA_NUMERO_PEDIDO:
                formatoOrigemString = FORMATO_DATAHORA_NUMERO_PEDIDO;
                break;
            case FORMATO_DATAHORA_PADRAO:
                formatoOrigemString = FORMATO_DATAHORA_PADRAO;
                break;
            case FORMATO_CALENDARIO:
                formatoOrigemString = FORMATO_CALENDARIO;
                break;
            default:
                formatoOrigemString = FORMATO_DATA_PADRAO;
                break;
        }

        switch (formatoDestino) {
            case FORMATO_DATA_BANCO:
                formatoDestinoString = FORMATO_DATA_BANCO;
                break;
            case FORMATO_DATA_PADRAO:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
            case FORMATO_DATAHORA_BANCO:
                formatoDestinoString = FORMATO_DATAHORA_BANCO;
                break;
            case FORMATO_DATAHORA_NUMERO_PEDIDO:
                formatoDestinoString = FORMATO_DATAHORA_NUMERO_PEDIDO;
                break;
            case FORMATO_DATAHORA_PADRAO:
                formatoDestinoString = FORMATO_DATAHORA_PADRAO;
                break;
            case FORMATO_CALENDARIO:
                formatoDestinoString = FORMATO_CALENDARIO;
                break;
            default:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
        }

        Date data;
        String resultado;
        SimpleDateFormat formOrigem = new SimpleDateFormat(formatoOrigemString, Locale.getDefault());
        SimpleDateFormat formDestino = new SimpleDateFormat(formatoDestinoString, Locale.getDefault());

        try {
            // Realiza o parse para data da string que veio via parâmetro
            data = formOrigem.parse(stringData);

            // Retorna a data em formato de data
            resultado = formDestino.format(data);
        } catch (ParseException e) {
            resultado = "Não foi possível realizar o parse da data(" + e.getMessage() + ")";
        }

        return resultado;
    }

    public static String formataData(Date data, FormatoData formatoDestino) {
        String formatoDestinoString;

        switch (formatoDestino) {
            case FORMATO_DATA_BANCO:
                formatoDestinoString = FORMATO_DATA_BANCO;
                break;
            case FORMATO_DATA_PADRAO:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
            case FORMATO_DATAHORA_BANCO:
                formatoDestinoString = FORMATO_DATAHORA_BANCO;
                break;
            case FORMATO_DATAHORA_NUMERO_PEDIDO:
                formatoDestinoString = FORMATO_DATAHORA_NUMERO_PEDIDO;
                break;
            case FORMATO_DATAHORA_PADRAO:
                formatoDestinoString = FORMATO_DATAHORA_PADRAO;
                break;
            case FORMATO_CALENDARIO:
                formatoDestinoString = FORMATO_CALENDARIO;
                break;
            default:
                formatoDestinoString = FORMATO_DATA_PADRAO;
                break;
        }

        //Date data;
        String resultado;
        SimpleDateFormat formDestino = new SimpleDateFormat(formatoDestinoString, Locale.getDefault());

        try {
            // Retorna a data em formato de data
            resultado = formDestino.format(data);
        } catch (Exception e) {
            resultado = "Não foi possível realizar a conversão da data(" + e.getMessage() + ")";
        }

        return resultado;
    }

    public static Calendar buscaCalendario() {
        // Retorna data formatada
        return Calendar.getInstance();
    }

    public static Date parseParaData(String stringData, FormatoData formatoOrigem) {
        String formatoOrigemString;

        switch (formatoOrigem) {
            case FORMATO_DATA_BANCO:
                formatoOrigemString = FORMATO_DATA_BANCO;
                break;
            case FORMATO_DATA_PADRAO:
                formatoOrigemString = FORMATO_DATA_PADRAO;
                break;
            case FORMATO_DATAHORA_BANCO:
                formatoOrigemString = FORMATO_DATAHORA_BANCO;
                break;
            case FORMATO_DATAHORA_NUMERO_PEDIDO:
                formatoOrigemString = FORMATO_DATAHORA_NUMERO_PEDIDO;
                break;
            case FORMATO_DATAHORA_PADRAO:
                formatoOrigemString = FORMATO_DATAHORA_PADRAO;
                break;
            case FORMATO_CALENDARIO:
                formatoOrigemString = FORMATO_CALENDARIO;
                break;
            default:
                formatoOrigemString = FORMATO_DATA_PADRAO;
                break;
        }

        Date data = new Date();
        SimpleDateFormat formOrigem = new SimpleDateFormat(formatoOrigemString, Locale.getDefault());

        try {
            // Realiza o parse para data da string que veio via parâmetro
            data = formOrigem.parse(stringData);

        } catch (ParseException e) {
            Log.d("mensagem", "Não foi possível realizar o parse da data(" + e.getMessage() + ")");
        }

        return data;
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static int retornaDiasUteis(Date dataInicial, Date dataFinal) {
        Calendar calendarioInicial = buscaCalendario();
        calendarioInicial.setTime(dataInicial);
        calendarioInicial.set(Calendar.HOUR_OF_DAY, 0);
        calendarioInicial.set(Calendar.MINUTE, 0);
        calendarioInicial.set(Calendar.SECOND, 0);
        calendarioInicial.set(Calendar.MILLISECOND, 0);

        Calendar caledarioFinal = buscaCalendario();
        caledarioFinal.setTime(dataFinal);
        caledarioFinal.set(Calendar.HOUR_OF_DAY, 0);
        caledarioFinal.set(Calendar.MINUTE, 0);
        caledarioFinal.set(Calendar.SECOND, 0);
        caledarioFinal.set(Calendar.MILLISECOND, 0);

        int diasUteis = 0;

        while (!isSameDay(calendarioInicial, caledarioFinal)) {
            if ((calendarioInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) && (calendarioInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)) {
                diasUteis++;
            }

            calendarioInicial.add(Calendar.DATE, 1);
        }

        if (isSameDay(calendarioInicial, caledarioFinal)) {
            if ((calendarioInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) && (calendarioInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)) {
                diasUteis++;
            }
        }

        return diasUteis;
    }

    public static boolean estaEmHorarioComercial() {
        int horaInicio = 8;
        int horaFim = 18;

        Calendar calendario = buscaCalendario();
        calendario.setTime(buscaDataAtual());
        int horaAtual = calendario.get(Calendar.HOUR_OF_DAY);
        int diaAtual = calendario.get(Calendar.DAY_OF_WEEK);

        boolean horarioComercial;

        if (horaAtual >= horaInicio && horaAtual <= horaFim) {
            switch (diaAtual) {
                case Calendar.SATURDAY:
                    horarioComercial = false;
                    break;
                case Calendar.SUNDAY:
                    horarioComercial = false;
                    break;
                default:
                    horarioComercial = true;
                    break;
            }
        } else {
            horarioComercial = false;
        }

        return horarioComercial;
    }
}