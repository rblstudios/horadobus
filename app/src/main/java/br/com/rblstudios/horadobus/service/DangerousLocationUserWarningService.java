package br.com.rblstudios.horadobus.service;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import br.com.rblstudios.horadobus.R;
import br.com.rblstudios.horadobus.ui.LoadingActivity;

/**
 * Criado por renan.lucas em 02/06/2017.
 */

public class DangerousLocationUserWarningService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location localizacaoPrecisa;
    private LocationListener listenerLocalizacao;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private final String DANGEROUS_LOCATIONS_CHILD = "DANGEROUS_LOCATIONS";
    private static final int NOTIFICATION_ID = 1;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;

    public DangerousLocationUserWarningService() {
        // Get DatabaseInstance and Reference
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("INFO", "Desconectando serviço google");
        // Desconectamos do serviço de API do google
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
        Log.i("INFO", "Destruindo serviço");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Criamos uma nova request de localização com prioridade de alta e tempo máximo de busca de 10s
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);

        // Listener para escutar uma resposta da busca feita no método "buscarLocalizacaoAtual()"
        listenerLocalizacao = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (ContextCompat.checkSelfPermission(DangerousLocationUserWarningService.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // Salvamos a localização encontrada
                    localizacaoPrecisa = location;
                    // Cancelamos atualizações, pois não precisamos mais delas neste momento
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listenerLocalizacao);
                    // Cadastramos no banco de dados
                    verificarLocalizacaoAlerta();
                }
            }
        };

        // Se já instanciamos o cliente de API do google
        if (mGoogleApiClient != null) {
            Log.i("Info", "Objeto já criado");
            // E está conectado, podemos buscar a localização
            if (mGoogleApiClient.isConnected()) {
                Log.i("Info", "Objeto já conectado. Cadastrando Loc");
                buscarLocalizacaoAtual();
            } else { // Caso não esteja conectado, conectamos
                Log.i("Info", "Objeto não conectado.Conectando");
                mGoogleApiClient.connect();
            }
        } else { // Se não tivermos instanciado o cliente, chamamos o método que faz esse instanciamento
            Log.i("Info", "Objeto nulo. Criando e conectando");
            conectaEmServicoGoogle();
        }

        return START_NOT_STICKY;
    }

    private void conectaEmServicoGoogle() {
        // Criamos uma instância do serviço de APIs do Google
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        Log.i("Info", "Criando objeto api google");
        // Conectamos a ele
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Ao conectar buscamos a localização
        buscarLocalizacaoAtual();
        Log.i("Info", "Conectado");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("Erro", "Conexão suspensa");
        // Se tivermos a conexão suspensa, paramos o serviço
        this.stopSelf();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Se a conexão falhar, paramos o serviço
        this.stopSelf();
        Log.e("Erro", "Conexão falhou");
    }

    private void verificarLocalizacaoAlerta() {
        Log.i("Info", "Entrando em metodo de cadastro de localização");

        Query query = databaseReference.child(DANGEROUS_LOCATIONS_CHILD);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Boolean deveMandarAlerta = false;
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>() {};
                    Map<String, Object> map = data.getValue(t);
                    if (map != null) {
                        LatLng latLng = new LatLng(Double.parseDouble(map.get("lat").toString()), Double.parseDouble(map.get("lon").toString()));
                        if (localizacaoPrecisa != null ) {
                            LatLng latLngUser = new LatLng(localizacaoPrecisa.getLatitude(), localizacaoPrecisa.getLongitude());

                            float[] results = new float[1];
                            Location.distanceBetween(latLngUser.latitude, latLngUser.longitude, latLng.latitude, latLng.longitude, results);
                            float distanceInMeters = results[0];
                            boolean isWithinRadius = distanceInMeters < Double.parseDouble(map.get("radius").toString());
                            if (isWithinRadius) {
                                // Mostra notificação
                                Log.i("Info", "Deve mandar alerta");
                                deveMandarAlerta = true;
                            } else {
                                Log.i("Info", "Não está em localização perigosa");
                            }
                        } else {
                            // Buscamos a ultima localização do aparelho para o caso de não acharmos nenhuma localização
                            if (ContextCompat.checkSelfPermission(DangerousLocationUserWarningService.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                Location localizacaoNaoPrecisa = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                if (localizacaoNaoPrecisa != null) {
                                    LatLng latLngUser = new LatLng(localizacaoNaoPrecisa.getLatitude(), localizacaoNaoPrecisa.getLongitude());

                                    float[] results = new float[1];
                                    Location.distanceBetween(latLngUser.latitude, latLngUser.longitude, latLng.latitude, latLng.longitude, results);
                                    float distanceInMeters = results[0];
                                    boolean isWithinRadius = distanceInMeters < Double.parseDouble(map.get("radius").toString());
                                    if (isWithinRadius) {
                                        Log.i("Info", "Deve mandar alerta");
                                        deveMandarAlerta = true;
                                    } else {
                                        Log.i("Info", "Não está em localização perigosa");
                                    }
                                }
                            }
                        }
                    }
                }

                if (deveMandarAlerta) {
                    exibirNotificacao("Atenção! Você está em um local considerado perigoso!");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Mostra notificação
                Log.d("DLUWS onCancelled", databaseError.getMessage());
            }
        });
    }

    private void buscarLocalizacaoAtual() {
        // Caso tenhamos permissão para buscar a localização, buscamos
        // Caso contrário, paramos o serviço
        if (ContextCompat.checkSelfPermission(DangerousLocationUserWarningService.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listenerLocalizacao);
        } else {
            this.stopSelf();
        }
    }

    private void exibirNotificacao(String mensagem) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_blue_bus)
                .setContentText(mensagem)
                .setContentTitle("Hora do Bus - Alerta")
                .setPriority(2);

        Intent retornoNotificacao = new Intent(this, LoadingActivity.class);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
        taskStackBuilder.addParentStack(LoadingActivity.class);

        taskStackBuilder.addNextIntent(retornoNotificacao);
        PendingIntent intentPendenteNotificacao = taskStackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(intentPendenteNotificacao);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
        Log.i("Info", "Notificação enviada com sucesso");
    }
}