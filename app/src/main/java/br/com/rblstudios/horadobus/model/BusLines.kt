package br.com.rblstudios.horadobus.model

import java.io.Serializable

/**
 * Created by renanlucas on 07/03/18.
 */
data class BusLines(
        val COD: String,
        val NOME: String,
        val SOMENTE_CARTAO: String,
        val CATEGORIA_SERVICO: String,
        val NOME_COR: String) : Serializable