package br.com.rblstudios.horadobus.model

import java.io.Serializable

/**
 * Created by renanlucas on 07/03/18.
 */
data class DangerousLocation(
        val LAT: Double = 0.0,
        val LON: Double = 0.0,
        val RADIUS: Double = 0.0,
        val Date: String = "",
        val BUS_LINE: String = "") : Serializable