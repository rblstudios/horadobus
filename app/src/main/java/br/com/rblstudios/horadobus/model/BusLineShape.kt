package br.com.rblstudios.horadobus.model

import java.io.Serializable

/**
 * Created by renanlucas on 07/03/18.
 */
data class BusLineShape(
        val SHP: String,
        val LAT: String,
        val LON: String,
        val COD: String) : Serializable